#!/bin/sh

# Copyright 2012  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Copyright 2013 Chess Griffin <chess.griffin@gmail.com> Raleigh, NC
# Copyright 2013-2018 Willy Sudiarto Raharjo <willysr@slackware-id.org>
# All rights reserved.
#
# Based on the xfce-build-all.sh script by Patrick J. Volkerding
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  --------------------------------------------------------------------------
#  MODIFIED BY FRANK HONOLKA <slackernetuk@gmail.com>
#  --------------------------------------------------------------------------
#

SNUKROOT=$(pwd)


# Check for duplicate sources (default: OFF)
CHECKDUPLICATE=0

src=(
ninja
meson
glib2
glibmm
poppler
vala
gobject-introspection
cantarell-fonts
gsettings-desktop-schemas
adwaita-icon-theme
bash-completion
gnome-keyring
pango
gtk4
libnma
mozjs91
polkit
gjs
upower
libadwaita
gsound
gnome-bluetooth
gnome-bluetooth3
xvfb-run
geocode-glib
libgweather
gnome-desktop
libdaemon
avahi
geoclue2
libgusb
colord
libgweather4
gnome-settings-daemon
mutter
gnome-menus
gnome-session
budgie-session
budgie-screensaver
uuid
libpeas
libportal
xdg-desktop-portal-gtk
xdg-desktop-portal-gnome
libhandy
libgnomekbd
xdg-dbus-proxy
libwpe
wpebackend-fdo
bubblewrap
webkit2gtk
rest
gnome-online-accounts
gnome-color-manager
gnome-bluetooth
cups-pk-helper
colord-gtk
libstemmer
appstream-glib
gnome-control-center
zenity
budgie-desktop
cogl
clutter
clutter-gst
clutter-gtk
cheese
budgie-control-center
jhead
budgie-backgrounds
budgie-desktop-view
vte
gnome-terminal
# Nemo file manager
tracker
libuchardet
totem-pl-parser
osinfo-db-tools
osinfo-db
libosinfo
libiptcdata
libgxps
exempi
tracker-miners
cinnamon-desktop
xapp
nemo
# themes
orchis-theme
murrine
materia-theme
papirus-icon-theme
Tela-icon-theme
plata-theme-bin
)

for dir in ${src[@]}; do

        # get package name
        package=$(echo $dir)

        # Change to package directory
        cd $SNUKROOT/sources/$dir || exit 1

        # Get the version
        version=$(cat ${package}.SlackBuild | grep "VERSION:" | head -n1 | cut -d "-" -f2 | rev | cut -c 2- | rev)

        # Get the build
        build=$(cat ${package}.SlackBuild | grep "BUILD:" | cut -d "-" -f2 | rev | cut -c 2- | rev)

        echo ${package}-${version}-${build} >> $SNUKROOT/PACKAGES.TXT || exit 1

        # back to original directory
        cd $SNUKROOT
done
