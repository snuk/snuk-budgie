# Budgie for Slackware 15.0

### Introduction

This is Budgie Desktop... from Scratch (for Slackware)

### Compatibilty
 
100% Compatible with slackware64-15.0

### Download and Install Compiled Packages
1. Download using this command:
**slackware64-15.0**
```bash
lftp -c mirror https://slackernet.ddns.net/slackware/slackware64-15.0/slackware64/budgie-desktop/ -c budgie-10.9-pkg64
```

2. As root, install:
```bash
upgradepkg --install-new --reinstall budgie-10.9-pkg64/*.txz
```

### Configuring
You need to add some groups and users to make things work better (I think), so:
1. In console (root), type:
```bash
groupadd -g 214 avahi
useradd -u 214 -g 214 -c "Avahi User" -d /dev/null -s /bin/false avahi
groupadd -g 303 colord
useradd -d /var/lib/colord -u 303 -g colord -s /bin/false colord
```
2. We need some startup services at boot, so edit your `/etc/rc.d/rc.local` adding these lines:
```bash
# Start avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
 /etc/rc.d/rc.avahidaemon start
fi
# Start avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd start
fi
```
3. Also stop the startup services at shutdown, so edit your `/etc/rc.d/rc.local_shutdown` adding these lines:
```bash
# Stop avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd stop
fi
# Stop avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
  /etc/rc.d/rc.avahidaemon stop
fi
```

Add to /etc/slackpkg/blacklist file

```bash
[0-9]+_snuk
```


As user setup Budgie Desktop as standard-session

```bash
xwmconfig
```

### How to compile and use Budgie Desktop 10.9  on Slackware 15.0 
 1. In console (root), type:
```bash
git clone https://codeberg.org/snuk/snuk-budgie.git -b main
cd snuk-budgie 
sh snuk
```
### Thanks
- [Slackware UK](http://slackware.uk/)
- [Arch Linux Team](https://www.archlinux.org/)
- [SlackBuilds Team](https://slackbuilds.org/)

### Contact.
 If you can help me to improve this project, please:
  - slackernetuk@gmail.com

I hope you enjoy it,

[Frank Honolka](https://www.facebook.com/frank.honolka.549/)

(2024, Great Britain)
